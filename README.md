# OpenFlexure Microscope Smart Stack Extension
This Python module provides "smart stack" functionality for the OpenFlexure Microscope: it uses a much cleverer algorithm to acquire Z stacks, ensuring that the central image is always the sharpest.  It's the work of Joe Knapper, and will be described in a forthcoming publication.

## Installing
We hope this will be simplified in due course by a better way of installing extensions for the OpenFlexure Microscope.  However, until that happens, you need to SSH in to your microscope (or open a terminal) and run:
```bash
git clone https://gitlab.com/jaknapper/smart-stack.git
cd /var/openflexure/extensions/microscope_extensions/
sudo chmod g+w .
ln -s /home/pi/smart-stack/smart_stack smart_stack
ofm restart
```
After doing this, the plugin should be available the next time you restart.

The interface will be included in the next beta release of the microscope software, in the "capture" tab.  To update your software, use the same terminal and type:
```bash
sudo ofm update
sudo ofm upgrade --pre
```
Currently, this doesn't yet have the interface - though it will be in `v2.10.0b2` very soon.  In the meantime, you can install it manually using:
```bash
cd /var/openflexure/application/openflexure-microscope-server/
sudo rm -rf openflexure_microscope/api/static/dist
curl https://gitlab.com/openflexure/openflexure-microscope-server/-/jobs/1508123513/artifacts/raw/dist/openflexure-microscope-webapp-smart-stack-controls.tar.gz | tar -xz
```
If you get permissions errors, do:
```bash
sudo chown -R openflexure-ws.openflexure-ws /var/openflexure
sudo chmod -R g+w /var/openflexure
```
## Using
You should have an extra section in the "capture" pane, marked "smart stack".  Tick the box to enable smart stack.  Parameters in the boxes above will set the scan geometry, filenames, etc. as with a regular scan.

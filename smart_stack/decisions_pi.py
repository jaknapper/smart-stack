import numpy as np
import numpy.polynomial.chebyshev as cheb
from scipy.interpolate import interp1d
import time
import logging
import os
import os.path
from openflexure_microscope.json import JSONEncoder
import json
import piexif
from labthings import (
    find_extension,
    find_component,
)
import numpy as np
import copy


def unpack(scan_data, start_index):
    """Extract z, sharpness data from a move_and_measure call
    
    Data will start at `start_index`, i.e. `start_index` points are dropped
    from the beginning of the array.
    """
    jpeg_times = scan_data['jpeg_times']
    jpeg_sizes = scan_data['jpeg_sizes']
    jpeg_sizes_MB = [x / 10**3 for x in jpeg_sizes]
    stage_times = scan_data['stage_times']
    stage_positions = scan_data['stage_positions']
    stage_height = [pos[2] for pos in stage_positions]

    jpeg_heights = np.interp(jpeg_times,stage_times,stage_height)

    return jpeg_heights[start_index:], jpeg_sizes_MB[start_index:]

def align(signal_heights, signal_sizes, full_scan_heights, full_scan_sizes):
    """Align two moves together.
    
    Given two (z, sharpness) curves, calculate the offset in z that makes
    them both match up the best.
    """
    top_score = np.inf
    top_offset = 0
    length = len(signal_heights)
    function = interp1d(full_scan_heights, full_scan_sizes, kind='cubic')
    full_height_range = max(full_scan_heights)-min(full_scan_heights)
    signal_height_range = max(signal_heights)-min(signal_heights)

    offsets = np.ceil(np.min(full_scan_heights)-np.min(signal_heights))
    while max(signal_heights) + offsets < max(full_scan_heights):
        score = np.sum(np.abs(np.subtract(signal_sizes, function(signal_heights+offsets))))
        if score < top_score:
            top_offset = offsets
            top_score = score
        offsets += 1
    return top_offset

def cross_thresh(x, y, thresh):
    """Check how many times and where a curve exceeds a threshold.
    
    `x`: x coordinates
    `y`: y coordinates
    `thresh`: The Y value to compare agains.
    
    Return value: count, crossing_points
    We return an integer count, and a list of crossing points.
    `count = len(crossing_points)`
    """
    current_level = 0
    previous_level = 0
    count = 0
    crossing_points = []
  
    # Iterate over the steps array 
    for i in range(1, len(y)): 
  
        previous_level = y[i-1] 
        current_level = y[i] 
  
        # Condition to check that the 
        # graph crosses the threshold
        if ((previous_level < thresh
            and current_level >= thresh) 
            or (previous_level > thresh
                and current_level <= thresh)): 
            count += 1
            crossing_points.append(y_inter(x[i-1:i+1], y[i-1:i+1], thresh))
  
    return count, crossing_points

# This function is not used - maybe delete it?
def find_thresh(x, y, target, steps, start, range_min, range_max):
    crosses = 0
    width = 0
    thresh = start
    while thresh >= min(y):
        crosses, crossing_points = cross_thresh(x, y, thresh)
        if crosses != 0:
            width = max(crossing_points) - min(crossing_points)
        if crosses == target and width > range_min and width < range_max:
            plt.plot(x, y)
            plt.axhline(y = thresh)
            plt.show()
            break
        if thresh - steps < min(y):
            return('No location found')
        else:
            thresh -= steps

def test_calibration_curve(heights, sharpness, threshold, width_min):
    """Check the peak is sufficiently narrow.
    
    Given a curve of sharpness vs z, check that it only exceeds the 
    threshold in a narrow range.
    """
    crosses, points = cross_thresh(heights, sharpness, threshold)
    if len(points) >= 2:
        if max(points) - min(points) < width_min:
            return True
        else:
            return False
    else:
        return False

def y_inter(x, y, point):
    """Interpolate the location where y=point, between two (x,y) points."""
    return x[1] - (x[1]- x[0]) * ((y[1] - point) / (y[1] - y[0]))

def test_stack(heights, sharpness, style):
    """Given a set of z, sharpness points, check it looks like it peaks.
    
    Here, the z, sharpness values should come from the positions where we
    acquire images in the Z stack.  We fit a curve to them, and check that
    it looks like it peaks in the middle.
    
    Return value: result, image_index
    `result` is a string, which will be one of:
      * `continue` if the peak is after the centre (so we should take more
        images)
      * `success` if the peak is in the middle of the range
      * `too far` if it looks like we have overshot the peak
      * `problem` if something else has gone wrong
    If `result` is `success`, `image_index` will be an integer.  Otherwise 
    it is the empty string.
    """
    dz = heights[1] - heights[0]
    centre_index = len(heights) // 2
    x = np.linspace(min(heights) - 1000, max(heights) + 1000, 1000)

    if style == 'quad':
        quad_est = np.polyfit(heights, sharpness, 2)
        sharpness_curve = np.polyval(quad_est, x)

        maximum = x[np.argmax(sharpness_curve)]
        if quad_est[0] > 0:
            return 'continue', ''
        elif maximum >= heights[centre_index] - 1.5 * dz and maximum <= heights[centre_index] + 1.5 * dz:
            
            image_index = np.argmin(np.abs(np.array(heights)-maximum))

            image_index = image_index - len(heights)
            
            return 'success', image_index
        elif maximum > heights[centre_index] + 1.5 * dz:
            return 'continue', ''
        elif maximum < heights[centre_index] - 1.5 * dz:
            print('uncentered')
            return 'too far'  , ''
        else:
            return 'problem', ''

    elif style == 'chevy':
        chevylevy = np.polynomial.chebyshev.chebfit(heights, sharpness, 4)
        sharpness_curve = cheb.chebval(x, chevylevy)

        der_chevy = cheb.chebder(chevylevy)
        dder_chevy = cheb.chebder(der_chevy)
        turning = cheb.chebroots(der_chevy)
        turning = turning[np.isreal(turning)]
        nature = np.asarray([np.real(cheb.chebval(point, dder_chevy)) for point in turning])
        maxima = turning[np.where(nature<0)]

        approach_grad = np.polyfit(heights[0:centre_index], sharpness[0:centre_index], 1)
        escape_grad = np.polyfit(heights[centre_index:-1], sharpness[centre_index:-1], 1)

        if approach_grad[0] < -20 and escape_grad[0] < -20:
            print('everything going down')
            return 'too far', ''

        if approach_grad[0] < 30:
            # print('Don\'t like the approach')
            return 'continue', ''

        if escape_grad[0] > -30:
            # print('Don\'t like the escape')
            return 'continue', ''

        peak_height = ''
        
        if np.count_nonzero(np.logical_and(np.real(turning) >= heights[1], np.real(turning) <= heights[-2])) == 1:
            if np.count_nonzero(np.logical_and(maxima >= (heights[centre_index] - 1.5 * dz), maxima<=(heights[centre_index] + 1.5 * dz))) == 1:
                for maximum in maxima:
                    if maximum >= heights[centre_index] - 1.5 * dz and maximum <= heights[centre_index] + 1.5 * dz:
                        peak_height = maximum
                
                image_index = np.argmin(np.abs(np.array(heights)-peak_height))
                image_index = image_index - len(heights)

                return 'success', image_index
            else: return 'continue', ''
        else: return 'continue', ''

    else: return 'problem', ''

def grab_image_size(microscope):
    """Calculate the size of a JPEG frame from the stream"""
    return len(microscope.camera.stream.getframe())

def delete_image(microscope, id_):
    """Delete an existing capture"""
    capture = microscope.captures.images.get(id_)
    if not capture:
        logging.warn(f"Tried to delete capture with ID '{id_}' but it was not found.")
        return
    capture.delete()
    del microscope.captures.images[id_]

def add_tags_to_image(microscope, id_, tags):
    """Add tags to a capture"""
    capture = microscope.captures.images.get(id_)
    capture.put_tags(tags)

class StackTracker:
    """This class keeps track of captures, sharpnesses, and heights of an ongoing stack"""
    def __init__(self, capture_kwargs=None, settling_time=.5):
        self.stack = []
        self.capture_kwargs = capture_kwargs or {}
        self.settling_time = settling_time

    def add_point(self):
        """Capture an image, and record the current position and sharpness
        Also capture a low res image"""
        time.sleep(self.settling_time)
        sharpness = grab_image_size(self.microscope)
        pos = self.microscope.stage.position
        stack_resolution = copy.copy(self.capture_kwargs['use_video_port'])

        capture = self.microscope.capture(
            filename="{0}_{1}/{2}".format(*pos),
            **self.capture_kwargs
        )
        self.capture_kwargs['use_video_port']= True
        low_res_capture = self.microscope.capture(
            filename="{0}_{1}/low_{2}_{3}".format(*pos, abs(round(time.time()) % 10000 )),
            **self.capture_kwargs
        )
        self.capture_kwargs['use_video_port']= stack_resolution
        self.stack.append((sharpness, pos, str(capture.id)))
        return self.stack[-1]

    @property
    def microscope(self):
        return find_component("org.openflexure.microscope")

    @property
    def heights(self):
        """A list of the heights of each image"""
        return [pos[2] for (_s, pos, _id) in self.stack]

    @property
    def sharpnesses(self):
        """A list of the heights of each image"""
        return [s for (s, _pos, _id) in self.stack]

    @property
    def capture_ids(self):
        """A list of the heights of each image"""
        return [capture_id for (_s, _pos, capture_id) in self.stack]

    def delete_all_captures(self):
        for _s, _pos, capture_id in self.stack:
            delete_image(self.microscope, capture_id)

    @property
    def data_dict(self):
        return {
            "sharpnesses": self.sharpnesses,
            "heights": self.heights,
            "capture_ids": self.capture_ids,
        }

    

def smart_stack(
    *,
    number_of_images, 
    dz, 
    microscope, 
    break_height, 
    min_sharp, 
    max_sharp, 
    style, 
    threshold, 
    capture_kwargs,
    settling_time
    ):
    """Take a Z stack, checking we are centred on the peak focus.
    
    This method will acquire the requested number of images, and keep going if it looks like the
    peak is above the centre of the current sweep.  After this method, we will either have
    9 images, nicely centred on the focus, or we will delete all the images and need to
    restart the sweep.

    This function does not do an autofocus - that's the responsibility of the calling code.
    """
    tracker = StackTracker(capture_kwargs=capture_kwargs, settling_time=settling_time)

    # Take the initial series of points
    for i in range(number_of_images):
        tracker.add_point()
        microscope.stage.move_rel((0,0,dz))

    while microscope.stage.position[2] < break_height:
        stack_height = tracker.heights[-number_of_images:]
        stack_sharpness = tracker.sharpnesses[-number_of_images:]
        # If the last n points are monotonically decreasing and the sharpest point is sharp (i.e. we're sure we missed the peak), abort.
        if max(tracker.sharpnesses) - min_sharp > threshold * (max_sharp - min_sharp) and sorted(stack_sharpness, reverse=True) == stack_sharpness:
            tracker.delete_all_captures()
            return 'Clearly decreasing', None, tracker.data_dict
        # If none of the sharpness values are high enough, keep moving onwards.
        elif max(stack_sharpness) - min_sharp < threshold * (max_sharp - min_sharp):
            print('not sharp enough')
            tracker.add_point()
            microscope.stage.move_rel((0,0,dz))
        # If neither obvious condition is met, test more carefully.
        else:
            stack_result, image_index = test_stack(stack_height, stack_sharpness, style)
            # print(stack_result)
            if stack_result == 'too far':
                tracker.delete_all_captures()
                return 'too far', None, tracker.data_dict
            elif stack_result == 'success':
                # Mark the central image with a tag
                add_tags_to_image(microscope, tracker.capture_ids[image_index], ['central_image'])
                # Delete images we no longer need - only keep `number_of_images`.
                for capture_id in tracker.capture_ids[0:-number_of_images]:
                    delete_image(microscope, capture_id)
                return 'success', image_index, tracker.data_dict
            elif stack_result == 'continue':
                tracker.add_point()
                microscope.stage.move_rel((0, 0, dz))
            else:
                tracker.delete_all_captures()
                return stack_result, None, tracker.data_dict
    tracker.delete_all_captures()
    return 'break height reached', None, tracker.data_dict

def bundle_result(result, image_index, report):
    """Ensure the result string appears in the report"""
    report["result"] = result
    report["image_index"] = image_index
    return result, image_index, report

def auto_stack(
    *,
    number_of_images, 
    dz, 
    threshold, 
    width, 
    microscope, 
    frange, 
    align_dist, 
    backlash,
    m_and_m_index, 
    **kwargs,
    ):
    """Perform an autofocus, and acquire a stack of images centred on the focus

    This function deals with the optimised fast autofocus - we sweep through
    the peak, then move back to the start and approach the peak, making a
    correction move based on the first sweep.  This should put us in the 
    right place for a good Z stack, using `smart_stack`.
    """
    autofocus_extension = find_extension("org.openflexure.autofocus")
    # We start by doing the first few steps of a `fast_autofocus`.
    microscope.stage.move_rel((0, 0, -frange / 2 - backlash - 400))
    microscope.stage.move_rel((0, 0, + backlash))
    c_data = autofocus_extension.move_and_measure(microscope, dz=frange)
    c_heights, c_sizes = unpack(c_data, m_and_m_index)
    c_sizes = [x * 1000 for x in c_sizes]
    report = {"calibration_move": c_data}

    # Check that we have a sharpness vs z curve with a nice peak
    cal_test = test_calibration_curve(c_heights, c_sizes, min(c_sizes) + threshold * (max(c_sizes) - min(c_sizes)), width)


    if cal_test is False:
        return bundle_result('Sample problem', None, report)
    else:
        # Move down below the peak and compensate for backlash
        full_peak_height = c_heights[np.argmax(c_sizes)]
        microscope.stage.move_rel((0, 0, -((microscope.stage.position[2] + align_dist + backlash + 240 + (number_of_images) * dz) - full_peak_height)))
        microscope.stage.move_rel((0, 0, backlash))

        # Move towards the peak, and estimate how much further we have to go
        a_data = autofocus_extension.move_and_measure(microscope, dz=align_dist)
        a_heights, a_sizes = unpack(a_data, m_and_m_index)
        a_sizes = [x * 1000 for x in a_sizes]
        report["approach_move"] = a_data
        
        lag = (align(a_heights, a_sizes, c_heights, c_sizes))
        report["lag"] = int(lag)
        
        if -2000 < lag < 2000:
            correction_step = (full_peak_height - (number_of_images/2) * dz) - microscope.stage.position[2] - lag - 51
            if correction_step < 0:
                return bundle_result("Too close to peak", None, report)
            else:
                microscope.stage.move_rel((0,0,correction_step))
                break_height = full_peak_height + (number_of_images + 15) * dz - lag
                result, image_index, stack_data = smart_stack(
                    number_of_images=number_of_images, 
                    dz=dz, 
                    microscope=microscope,
                    break_height=break_height, 
                    min_sharp=min(c_sizes),
                    max_sharp=max(c_sizes), 
                    threshold=threshold, 
                    **kwargs
                )
                report["stack_data"] = stack_data
                return bundle_result(result, image_index, report)
        else:
            return bundle_result('lag problem', None, report)

def stack(
        *,
        microscope,
        number_of_images, 
        scan_dir,
        **kwargs
    ):
    """Top-level smart stack routine
    
    This function will perform an optimised fast autofocus, then take a Z stack,
    restarting as needed to ensure we get a good stack, centred on the focus.
    
    This function is also responsible for writing out the JSON reports to disk.
    """
    start_t = time.time()
    
    report = {
        "stack_reports": [],
        }
    attempts = 0
    result = "not run yet"
    while result != "success":
        if result != "not run yet":
            print('{0}, retrying'.format(result))
            find_extension("org.openflexure.autofocus").fast_autofocus(microscope) #prevents an infinite loop (hopefully!)
        result, image_index, stack_report = auto_stack(
            microscope=microscope,
            number_of_images = number_of_images,
            **kwargs
        )
        report['stack_reports'].append(stack_report)
        logging.info(f"Stack number {attempts+1} result was {result}")
        attempts += 1
        if attempts >= 5:
            break
    
        report['overall'] = {
            'failed stacks': attempts - 1 if result == "success" else attempts,
            'time taken': int(time.time() - start_t),
            'image_index': image_index,
            'location': (microscope.stage.position[0], microscope.stage.position[1])
        }

        try:
            stack_dir = os.path.join(scan_dir, "{0}_{1}".format(microscope.stage.position[0], microscope.stage.position[1]))
            os.makedirs(stack_dir)
        except FileExistsError:
            pass # If the directory exists, no problem!

        logging.info(f"Writing stack report as data.json to {stack_dir}.")
        with open(os.path.join(stack_dir, 'data.json'), 'w') as fp:
            fp.write(JSONEncoder(indent=4).encode(report))

    print('This stack took {0} seconds'.format(int(time.time() - start_t)))
    if result != "success":
        logging.info(f"Smart stack failed after {attempts} attempts")
        raise Exception(f"Smart stack failed after {attempts} attempts")
    else:
        logging.info(f"Smart stack succeeded after {attempts} attempts")
    return result, image_index, report

def check_ascending(sharpness_list):
    if sorted(sharpness_list) == sharpness_list:
        return True

def check_descending(sharpness_list):
    if sorted(sharpness_list, reverse=True) == sharpness_list:
        return True

def turningpoints(x):
    N=0
    for i in range(1, len(x)-1):
        if ((x[i-1] < x[i] and x[i+1] < x[i]) or (x[i-1] > x[i] and x[i+1] > x[i])):
            N += 1
    return N

def construct_grid(initial, step_sizes, n_steps, style="raster"):
    
    arr = []

    if style == "spiral":
        # deal with the centre image immediately
        coord = initial
        arr.append(initial)
        # for spiral, n_steps is the number of shells, and so only requires n_steps[0]
        for i in range(2, n_steps[0]+1):
            arr.append([])
            side_length = (2 * i) - 1

            # iteratively generate the next location to append
            coord = [coord[ax] + [-1,1][ax] * step_sizes[ax] for ax in range(2)]
            for direction in ([1,0],[0,-1],[-1,0],[0,1]):
                for edge_images in range(side_length-1):
                    coord = [coord[ax] + direction[ax] * step_sizes[ax] for ax in range(2)]
                    arr[i-1].append(tuple(coord))

        # print(arr)
        grid = [item for sublist in arr[1:] for item in sublist]
        grid.insert(0, arr[0])

    else:
        for i in range(n_steps[0]):  # x axis
            arr.append([])
            for j in range(n_steps[1]):  # y axis
                # Create a coordinate array
                coord = [initial[ax] + [i, j][ax] * step_sizes[ax] for ax in range(2)]
                # Append coordinate array to position grid
                arr[i].append(tuple(coord))

        # Style modifiers
        if style == "snake":
            for i, line in enumerate(arr):
                if i % 2 != 0:
                    line.reverse()
        grid = [item for sublist in arr for item in sublist]
    
    return grid

def pull_usercomment_dict(im_link):
    """
    Reads UserComment Exif data from a file, and returns the contained bytes as a pandas dataframe.
    If any keys in params aren't included, they are appended with value None
    Args:
        im_link: Path to the Exif-containing file
        params: A list of all the parameters we'd expect from a recent scan
    """
    exif_dict = piexif.load(im_link)
    if "Exif" in exif_dict and piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        j = json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
        return(j)

        # my_dataframe['image_acquisitionDate'][0] = datetime.datetime.fromisoformat(my_dataframe['image_acquisitionDate'][0]).strftime('%Y-%m-%d_%H-%M-%S')
        # try:
        #     my_dataframe['dataset_acquisitionDate'][0] = datetime.datetime.fromisoformat(my_dataframe['dataset_acquisitionDate'][0]).strftime('%Y-%m-%d_%H-%M-%S')
        # except:
        #     pass
    else:
        raise ValueError('JSON is empty')

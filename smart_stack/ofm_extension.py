import datetime
import json
import logging
import time
import uuid
import marshmallow
import os
from functools import reduce
from typing import Dict, List, Optional, Tuple
from openflexure_microscope.api.default_extensions.scan import TileScanArgs, construct_grid
from openflexure_microscope.json import JSONEncoder


from labthings import (
    current_action,
    fields,
    find_component,
    find_extension,
    update_action_progress,
)
from labthings.extensions import BaseExtension
from labthings.views import ActionView
from typing_extensions import Literal

from openflexure_microscope.api.v2.views.actions.camera import FullCaptureArgs
from openflexure_microscope.captures.capture_manager import generate_basename
from openflexure_microscope.devel import abort
from openflexure_microscope.microscope import Microscope
from openflexure_microscope.api.v2.views.captures import CaptureView, CaptureSchema, CaptureTags

from . import decisions_pi
from . import __version__

# Type alias for convenience
XyCoordinate = Tuple[int, int]
XyzCoordinate = Tuple[int, int, int]

class EmulatedMicroscopeClient():
    """Allow microscope-client code to work with a real microscope
    
    At the moment, the python client module doesn't match exactly with
    the Python API of the microscope.  This wrapper provides just enough
    functionality that Joe's autofocus code should now work with either.
    """
    def __init__(self, microscope):
        self.microscope = microscope

    def move_rel(self, pos):
        self.microscope.stage.move_rel(pos)
    
    def move_and_measure(self, args):
        autofocus_extension = find_extension("org.openflexure.autofocus")
        return autofocus_extension.move_and_measure(self.microscope, dz=args.get("dz"))
    
    def autofocus(self):
        autofocus_extension = find_extension("org.openflexure.autofocus")
        return autofocus_extension.fast_autofocus(self.microscope)

    def grab_image_size(self):
        return len(self.microscope.camera.stream.getframe())

    @property
    def position(self):
        return self.microscope.stage.position_map

    def capture_image_to_disk(self, params):
        # TODO: sanitize args, e.g. convert resize from dict to tuple
        capture = self.microscope.capture(**params)
        return CaptureSchema().dump(capture)

    def delete_image(self, id):
        CaptureView().delete(id)

    def put_tag(self, id, tags):
        capture_obj = self.microscope.captures.images.get(id)
        capture_obj.put_tags(tags)
    
    

class SmartStackExtension(BaseExtension):
    def __init__(self):
        BaseExtension.__init__(self, "org.openflexure.smart-stack", version=__version__)

        self._images_to_be_captured: int = 1
        self._images_captured_so_far: int = 0

        self.add_view(SmartScanAPI, "/tile", endpoint="tile")

    def tile(
        self,
        microscope: Microscope,
        *,
        basename: Optional[str] = None,
        stride_size: XyzCoordinate = (2000, 1500, 100),
        grid: XyzCoordinate = (3, 3, 5),
        style="raster",
        autofocus_dz: int = 50,
        threshold = 0.9,
        width = 200,
        align_dist = 900,
        backlash = 100,
        fit_style = "chevy",
        m_and_m_index = 10,
        settling_time,
        **kwargs, #passed to capture
    ):
        """Make a tiled scan, using smart stacks at each location"""
        # kwargs is passed to the capture method - so we check it's not got extra keys.
        for k in kwargs.keys():
            if k not in FullCaptureArgs().fields.keys():
                raise KeyError("A keyword argument was passed that is not a capture argument.")

        start = time.time()

        # Generate sensible filenames
        if not basename:
            basename = generate_basename()
        folder_name = f"SMART_SCAN_{basename}"
        base_path = microscope.captures.paths["temp" if kwargs.get("temporary", False) else "default"]
        scan_dir = os.path.join(base_path, folder_name)
        try:
            os.makedirs(scan_dir)
        except FileExistsError:
            abort(400, "The filename you specified already exists.")

        # Store initial position
        initial_position = microscope.stage.position

        # Add dataset metadata
        capture_kwargs = {
            "folder": folder_name,
            "cache_key": folder_name,
            **kwargs
        }
        capture_kwargs["dataset"] = {
            "id": uuid.uuid4(),
            "type": "smartStackScan",
            "name": basename,
            "acquisitionDate": datetime.datetime.now().isoformat(),
            "strideSize": stride_size,
            "grid": grid,
            "style": style,
            "autofocusDz": autofocus_dz,
            "threshold": threshold,
            "width": width,
            "align_dist": align_dist,
            "backlash": backlash,
            "fit_style": fit_style,
            "m_and_m_index": m_and_m_index,
            "settling_time": settling_time,
        }

        # Check if autofocus is enabled (disabled for now - no point if it's not enabled!)
        autofocus_extension = find_extension("org.openflexure.autofocus")

        # Construct an x-y grid (worry about z later)
        x_y_grid = construct_grid(
            initial_position[:2], stride_size[:2], grid[:2], style=style
        )

        # Keep the initial Z position the same as our current position
        initial_z = initial_position[2]
        next_z = initial_z  # Save this value for use in raster scans

        logging.info(f"Starting a smart stack and scan, {style} shape, with {grid} images")
        # Start by running an autofocus
        autofocus_extension.fast_autofocus(microscope, dz=autofocus_dz)
        try:
            # Now step through each point in the x-y coordinate array
            for line in x_y_grid:
                # If rastering, rather than snake (or eventually spiral)
                # Return focus to initial position
                if style == "raster":
                    next_z = initial_z  # Reset z position at start of each new row
                    logging.debug("Returning to initial z position")
                    microscope.stage.move_abs(
                        (line[0][0], line[0][1], next_z)
                    )  # RWB: I think this line is redundant

                for x_y in line:
                    # Move to new grid position without changing z
                    logging.debug("Moving to step %s", ([x_y[0], x_y[1], next_z]))
                    microscope.stage.move_abs((x_y[0], x_y[1], next_z))
                    
                    logging.debug("Entering z-stack")
                    decisions_pi.stack(
                        microscope=microscope,
                        dz=stride_size[2],
                        number_of_images=grid[2],
                        threshold=threshold,
                        width=width,
                        align_dist=align_dist,
                        backlash=backlash,
                        style=fit_style,
                        m_and_m_index=m_and_m_index,
                        capture_kwargs = capture_kwargs,
                        frange = autofocus_dz,
                        scan_dir = scan_dir,
                        settling_time = settling_time,
                    )
                if current_action() and current_action().stopped:
                    return
                # Make sure we use our current best estimate of focus (i.e. the current position) next point
                next_z = microscope.stage.position[2]
        finally:
            logging.debug("Returning to %s", (initial_position))
            microscope.stage.move_abs(initial_position)

            end = time.time()
            logging.info("Scan took %s seconds", round(end - start))

            output = {
                'stack_images': grid[2],
                'dz': stride_size[2],
                'threshold': threshold,
                'width': width,
                'focus_range': autofocus_dz,
                'align_dist': align_dist,
                'backlash': backlash,
                'style': fit_style,
                'move_and_measure_start': m_and_m_index,
                'initial_coords': initial_position,
                'dx, dy': stride_size[:2],
                'grid_steps': stride_size[:2],
                'scan_style': style,
                'time_taken': int(time.time() - start),
            }
            with open(os.path.join(scan_dir, 'data.json'), 'w') as fp:
                fp.write(JSONEncoder(indent=4).encode(output))


LABTHINGS_EXTENSIONS = [SmartStackExtension]


class SmartScanArgs(TileScanArgs):
    #TODO currently fast_autofocus gets ignored...
    threshold = fields.Float(missing=0.9, example=0.9)
    width = fields.Integer(missing=200, example=200)
    align_dist = fields.Integer(missing=900, example=900)
    backlash = fields.Integer(missing=100, example=100)
    fit_style = fields.String(missing="chevy", example="chevy")
    m_and_m_index = fields.Integer(missing=10, example=10)
    # These are already in TileScanArgs, but we want
    # to change the default so it's fast autofocus
    autofocus_dz = fields.Integer(missing=3000, example=3000)
    fast_autofocus = fields.Boolean(missing=True, example=True)
    settling_time = fields.Float(missing=1.5, example=1.5)



class SmartScanAPI(ActionView):
    args = SmartScanArgs()

    # Allow 10 seconds to stop upon DELETE request
    # Gives fast-autofocus time to finish if it's running
    default_stop_timeout = 10

    def post(self, args):
        microscope = find_component("org.openflexure.microscope")

        if not microscope:
            abort(503, "No microscope connected. Unable to run smart stack.")
        if not microscope.has_real_stage():
            abort(503, "Cannot do smart stack without a real stage.")
        if not microscope.has_real_camera():
            abort(503, "A real camera is required for smart stack.")
        if not find_extension("org.openflexure.autofocus"):
            abort(503, "The autofocus extension is required by smart stack.")

        resize = args.get("resize", None)
        if resize:
            if ("width" in resize) and ("height" in resize):
                resize = (
                    int(resize["width"]),
                    int(resize["height"]),
                )  # Convert dict to tuple
            else:
                abort(400, "Resize must contain integers for width and height")

        if not args.get("fast_autofocus"):
            abort(400, "Smart stack only works with fast_autofocus enabled.")

        if args.get("namemode") != "coordinates":
            abort(400, "Smart stack only works with namemode='coordinates'")
        
        if args.get("grid")[2] != 9:
            logging.warn(f"Smart stack is only tested with 9 images so far and we have {args.get('grid')[2]}.")

        if args.get("grid")[2] < 5:
            abort(400, "Smart stack requires a stack of images to test for focus, minimum 5 and designed for 9.")
        #TODO: warn if z range (grid[2]*stride_size[2]) is too big?


        kwargs = {
            k: args.get(k) for k in self.args.fields.keys()
                           if k not in ["filename", "resize"]
        }
        kwargs["basename"] = args.get("filename")
        kwargs["resize"] = resize
        del kwargs["fast_autofocus"] # These arguments are ignored, and warned above if they are wrong.
        del kwargs["namemode"]

        logging.info("Running tile scan...")
        # Acquire microscope lock with 1s timeout
        with microscope.lock(timeout=1):
            # Run scan_extension_v2
            return self.extension.tile(microscope, **kwargs)